﻿namespace ResizeToll
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxInputFolder = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.textBoxOutputFolder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxW = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxH = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxClearFolder = new System.Windows.Forms.CheckBox();
            this.labelTotalInput = new System.Windows.Forms.Label();
            this.labelTotalOut = new System.Windows.Forms.Label();
            this.checkBoxHoodie = new System.Windows.Forms.CheckBox();
            this.tabControlResizeTool = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonCleanResizedDir = new System.Windows.Forms.Button();
            this.checkBoxSkipUploadDrive = new System.Windows.Forms.CheckBox();
            this.dataGridViewDriveDone = new System.Windows.Forms.DataGridView();
            this.GCopy = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label42 = new System.Windows.Forms.Label();
            this.buttonDownloadAndExport = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.richTextBoxImageNames = new System.Windows.Forms.RichTextBox();
            this.labelCurrentImageName = new System.Windows.Forms.Label();
            this.buttonFromClipboard = new System.Windows.Forms.Button();
            this.labelDpiX = new System.Windows.Forms.Label();
            this.labelDpiY = new System.Windows.Forms.Label();
            this.labelH = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelW = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxDpiY = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxDpiX = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxHeight = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxWidth = new System.Windows.Forms.TextBox();
            this.buttonExport = new System.Windows.Forms.Button();
            this.labelProcessFile = new System.Windows.Forms.Label();
            this.labelTotalFile = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxFolderTeescape = new System.Windows.Forms.TextBox();
            this.tabPageResizeTool = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonReplaceName = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxMinFile = new System.Windows.Forms.TextBox();
            this.buttonStartClean = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxInputCleanDirectory = new System.Windows.Forms.TextBox();
            this.labelTotalDeleted = new System.Windows.Forms.Label();
            this.tabPageGoogleDrive = new System.Windows.Forms.TabPage();
            this.checkBoxFirebaseTracking = new System.Windows.Forms.CheckBox();
            this.checkBoxMergeAfterDone = new System.Windows.Forms.CheckBox();
            this.textBoxReplaceBy = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxFindText = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxTrackingFilePath = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBoxOutputDriveRename = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxParentFolderDrive = new System.Windows.Forms.TextBox();
            this.buttonRenameDrive = new System.Windows.Forms.Button();
            this.labelDownloaded = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxDownloadTo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxAppName = new System.Windows.Forms.TextBox();
            this.checkBoxDeleteAfterDown = new System.Windows.Forms.CheckBox();
            this.buttonDriveDownload = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxDriveGuid = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.textboxResult = new System.Windows.Forms.Label();
            this.textBoxConvertResult = new System.Windows.Forms.TextBox();
            this.buttonStartConvert = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxDateInput = new System.Windows.Forms.TextBox();
            this.googleFeedTab = new System.Windows.Forms.TabPage();
            this.checkBoxIsBingAds = new System.Windows.Forms.CheckBox();
            this.labelCounter = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxRange = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxDBName = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxInterval = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxSiteAddress = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxDBPass = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxDBUser = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBoxDBIP = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxSheetId = new System.Windows.Forms.TextBox();
            this.buttonStartFeed = new System.Windows.Forms.Button();
            this.tabPageCSVImport = new System.Windows.Forms.TabPage();
            this.labelTotalCSVDone = new System.Windows.Forms.Label();
            this.labelTotalCSV = new System.Windows.Forms.Label();
            this.checkBoxIsFullPathExport = new System.Windows.Forms.CheckBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBoxImportCsvEndpoint = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.textBoxCsvUploadFolderPath = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBoxCSVPass = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxCSVUser = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBoxDBIP_csv = new System.Windows.Forms.TextBox();
            this.buttonImportCSV = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.flowLayoutPanelButton = new System.Windows.Forms.FlowLayoutPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.comboBoxProfile = new System.Windows.Forms.ComboBox();
            this.buttonSaveProfile = new System.Windows.Forms.Button();
            this.buttonRemoveProfile = new System.Windows.Forms.Button();
            this.buttonCleanDuplicatedFile = new System.Windows.Forms.Button();
            this.tabControlResizeTool.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDriveDone)).BeginInit();
            this.tabPageResizeTool.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPageGoogleDrive.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.googleFeedTab.SuspendLayout();
            this.tabPageCSVImport.SuspendLayout();
            this.flowLayoutPanelButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input folder:";
            // 
            // textBoxInputFolder
            // 
            this.textBoxInputFolder.Location = new System.Drawing.Point(82, 11);
            this.textBoxInputFolder.Name = "textBoxInputFolder";
            this.textBoxInputFolder.Size = new System.Drawing.Size(270, 20);
            this.textBoxInputFolder.TabIndex = 1;
            this.textBoxInputFolder.TextChanged += new System.EventHandler(this.textBoxInputFolder_TextChanged);
            // 
            // buttonStart
            // 
            this.buttonStart.BackColor = System.Drawing.Color.LimeGreen;
            this.buttonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStart.ForeColor = System.Drawing.Color.White;
            this.buttonStart.Location = new System.Drawing.Point(254, 93);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(98, 52);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = false;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // textBoxOutputFolder
            // 
            this.textBoxOutputFolder.Location = new System.Drawing.Point(82, 39);
            this.textBoxOutputFolder.Name = "textBoxOutputFolder";
            this.textBoxOutputFolder.Size = new System.Drawing.Size(270, 20);
            this.textBoxOutputFolder.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Output folder:";
            // 
            // textBoxW
            // 
            this.textBoxW.Location = new System.Drawing.Point(82, 67);
            this.textBoxW.Name = "textBoxW";
            this.textBoxW.Size = new System.Drawing.Size(69, 20);
            this.textBoxW.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "New sizes:";
            // 
            // textBoxH
            // 
            this.textBoxH.Location = new System.Drawing.Point(168, 67);
            this.textBoxH.Name = "textBoxH";
            this.textBoxH.Size = new System.Drawing.Size(69, 20);
            this.textBoxH.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(79, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "(4500 x 5400, 4500 x 4050)";
            // 
            // checkBoxClearFolder
            // 
            this.checkBoxClearFolder.AutoSize = true;
            this.checkBoxClearFolder.Location = new System.Drawing.Point(254, 70);
            this.checkBoxClearFolder.Name = "checkBoxClearFolder";
            this.checkBoxClearFolder.Size = new System.Drawing.Size(102, 17);
            this.checkBoxClearFolder.TabIndex = 9;
            this.checkBoxClearFolder.Text = "Clear output first";
            this.checkBoxClearFolder.UseVisualStyleBackColor = true;
            // 
            // labelTotalInput
            // 
            this.labelTotalInput.AutoSize = true;
            this.labelTotalInput.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalInput.ForeColor = System.Drawing.Color.OrangeRed;
            this.labelTotalInput.Location = new System.Drawing.Point(358, 11);
            this.labelTotalInput.Name = "labelTotalInput";
            this.labelTotalInput.Size = new System.Drawing.Size(23, 23);
            this.labelTotalInput.TabIndex = 10;
            this.labelTotalInput.Text = "0";
            // 
            // labelTotalOut
            // 
            this.labelTotalOut.AutoSize = true;
            this.labelTotalOut.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalOut.ForeColor = System.Drawing.Color.LimeGreen;
            this.labelTotalOut.Location = new System.Drawing.Point(358, 41);
            this.labelTotalOut.Name = "labelTotalOut";
            this.labelTotalOut.Size = new System.Drawing.Size(23, 23);
            this.labelTotalOut.TabIndex = 12;
            this.labelTotalOut.Text = "0";
            // 
            // checkBoxHoodie
            // 
            this.checkBoxHoodie.AutoSize = true;
            this.checkBoxHoodie.Location = new System.Drawing.Point(82, 117);
            this.checkBoxHoodie.Name = "checkBoxHoodie";
            this.checkBoxHoodie.Size = new System.Drawing.Size(58, 17);
            this.checkBoxHoodie.TabIndex = 13;
            this.checkBoxHoodie.Text = "hoodie";
            this.checkBoxHoodie.UseVisualStyleBackColor = true;
            this.checkBoxHoodie.CheckedChanged += new System.EventHandler(this.checkBoxHoodie_CheckedChanged);
            // 
            // tabControlResizeTool
            // 
            this.tabControlResizeTool.Controls.Add(this.tabPage3);
            this.tabControlResizeTool.Controls.Add(this.tabPageResizeTool);
            this.tabControlResizeTool.Controls.Add(this.tabPage2);
            this.tabControlResizeTool.Controls.Add(this.tabPageGoogleDrive);
            this.tabControlResizeTool.Controls.Add(this.tabPage1);
            this.tabControlResizeTool.Controls.Add(this.googleFeedTab);
            this.tabControlResizeTool.Controls.Add(this.tabPageCSVImport);
            this.tabControlResizeTool.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlResizeTool.Location = new System.Drawing.Point(0, 0);
            this.tabControlResizeTool.Name = "tabControlResizeTool";
            this.tabControlResizeTool.SelectedIndex = 0;
            this.tabControlResizeTool.Size = new System.Drawing.Size(570, 409);
            this.tabControlResizeTool.TabIndex = 14;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.buttonCleanResizedDir);
            this.tabPage3.Controls.Add(this.checkBoxSkipUploadDrive);
            this.tabPage3.Controls.Add(this.dataGridViewDriveDone);
            this.tabPage3.Controls.Add(this.label42);
            this.tabPage3.Controls.Add(this.buttonDownloadAndExport);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.richTextBoxImageNames);
            this.tabPage3.Controls.Add(this.labelCurrentImageName);
            this.tabPage3.Controls.Add(this.buttonFromClipboard);
            this.tabPage3.Controls.Add(this.labelDpiX);
            this.tabPage3.Controls.Add(this.labelDpiY);
            this.tabPage3.Controls.Add(this.labelH);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.labelW);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.textBoxDpiY);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.textBoxDpiX);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.textBoxHeight);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.textBoxWidth);
            this.tabPage3.Controls.Add(this.buttonExport);
            this.tabPage3.Controls.Add(this.labelProcessFile);
            this.tabPage3.Controls.Add(this.labelTotalFile);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.textBoxFolderTeescape);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(562, 383);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "TeescapeUtils";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // buttonCleanResizedDir
            // 
            this.buttonCleanResizedDir.BackColor = System.Drawing.Color.Orange;
            this.buttonCleanResizedDir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCleanResizedDir.Location = new System.Drawing.Point(17, 262);
            this.buttonCleanResizedDir.Name = "buttonCleanResizedDir";
            this.buttonCleanResizedDir.Size = new System.Drawing.Size(67, 25);
            this.buttonCleanResizedDir.TabIndex = 59;
            this.buttonCleanResizedDir.Text = "Cleanup";
            this.buttonCleanResizedDir.UseVisualStyleBackColor = false;
            this.buttonCleanResizedDir.Click += new System.EventHandler(this.buttonCleanResizedDir_Click);
            // 
            // checkBoxSkipUploadDrive
            // 
            this.checkBoxSkipUploadDrive.AutoSize = true;
            this.checkBoxSkipUploadDrive.Location = new System.Drawing.Point(17, 239);
            this.checkBoxSkipUploadDrive.Name = "checkBoxSkipUploadDrive";
            this.checkBoxSkipUploadDrive.Size = new System.Drawing.Size(106, 17);
            this.checkBoxSkipUploadDrive.TabIndex = 58;
            this.checkBoxSkipUploadDrive.Text = "Skip Goole Drive";
            this.checkBoxSkipUploadDrive.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDriveDone
            // 
            this.dataGridViewDriveDone.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridViewDriveDone.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridViewDriveDone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDriveDone.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GCopy});
            this.dataGridViewDriveDone.Location = new System.Drawing.Point(185, 166);
            this.dataGridViewDriveDone.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewDriveDone.Name = "dataGridViewDriveDone";
            this.dataGridViewDriveDone.RowHeadersWidth = 62;
            this.dataGridViewDriveDone.RowTemplate.Height = 28;
            this.dataGridViewDriveDone.Size = new System.Drawing.Size(373, 125);
            this.dataGridViewDriveDone.TabIndex = 57;
            this.dataGridViewDriveDone.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDriveDone_CellContentDoubleClick);
            // 
            // GCopy
            // 
            this.GCopy.HeaderText = "Copy";
            this.GCopy.MinimumWidth = 8;
            this.GCopy.Name = "GCopy";
            this.GCopy.ReadOnly = true;
            this.GCopy.Text = "Copy";
            this.GCopy.Width = 37;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(185, 151);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(89, 13);
            this.label42.TabIndex = 56;
            this.label42.Text = "Drive output links";
            // 
            // buttonDownloadAndExport
            // 
            this.buttonDownloadAndExport.BackColor = System.Drawing.Color.DarkOrchid;
            this.buttonDownloadAndExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDownloadAndExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDownloadAndExport.ForeColor = System.Drawing.Color.White;
            this.buttonDownloadAndExport.Location = new System.Drawing.Point(141, 296);
            this.buttonDownloadAndExport.Name = "buttonDownloadAndExport";
            this.buttonDownloadAndExport.Size = new System.Drawing.Size(105, 44);
            this.buttonDownloadAndExport.TabIndex = 54;
            this.buttonDownloadAndExport.Text = "Download S3  Xuất Hình";
            this.buttonDownloadAndExport.UseVisualStyleBackColor = false;
            this.buttonDownloadAndExport.Click += new System.EventHandler(this.buttonDownloadAndExport_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(189, 68);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(146, 13);
            this.label22.TabIndex = 53;
            this.label22.Text = "S3 Tên hình cách nhau dấu ,";
            // 
            // richTextBoxImageNames
            // 
            this.richTextBoxImageNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBoxImageNames.Location = new System.Drawing.Point(185, 83);
            this.richTextBoxImageNames.Name = "richTextBoxImageNames";
            this.richTextBoxImageNames.Size = new System.Drawing.Size(375, 64);
            this.richTextBoxImageNames.TabIndex = 52;
            this.richTextBoxImageNames.Text = "";
            // 
            // labelCurrentImageName
            // 
            this.labelCurrentImageName.AutoSize = true;
            this.labelCurrentImageName.Location = new System.Drawing.Point(265, 296);
            this.labelCurrentImageName.Name = "labelCurrentImageName";
            this.labelCurrentImageName.Size = new System.Drawing.Size(16, 13);
            this.labelCurrentImageName.TabIndex = 51;
            this.labelCurrentImageName.Text = "...";
            // 
            // buttonFromClipboard
            // 
            this.buttonFromClipboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFromClipboard.Location = new System.Drawing.Point(301, 18);
            this.buttonFromClipboard.Name = "buttonFromClipboard";
            this.buttonFromClipboard.Size = new System.Drawing.Size(84, 26);
            this.buttonFromClipboard.TabIndex = 50;
            this.buttonFromClipboard.Text = "From Clipboard";
            this.buttonFromClipboard.UseVisualStyleBackColor = true;
            this.buttonFromClipboard.Click += new System.EventHandler(this.buttonFromClipboard_Click);
            // 
            // labelDpiX
            // 
            this.labelDpiX.AutoSize = true;
            this.labelDpiX.Location = new System.Drawing.Point(296, 320);
            this.labelDpiX.Name = "labelDpiX";
            this.labelDpiX.Size = new System.Drawing.Size(13, 13);
            this.labelDpiX.TabIndex = 43;
            this.labelDpiX.Text = "0";
            // 
            // labelDpiY
            // 
            this.labelDpiY.AutoSize = true;
            this.labelDpiY.Location = new System.Drawing.Point(346, 320);
            this.labelDpiY.Name = "labelDpiY";
            this.labelDpiY.Size = new System.Drawing.Size(13, 13);
            this.labelDpiY.TabIndex = 45;
            this.labelDpiY.Text = "0";
            // 
            // labelH
            // 
            this.labelH.AutoSize = true;
            this.labelH.Location = new System.Drawing.Point(441, 320);
            this.labelH.Name = "labelH";
            this.labelH.Size = new System.Drawing.Size(13, 13);
            this.labelH.TabIndex = 49;
            this.labelH.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(425, 320);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 13);
            this.label13.TabIndex = 48;
            this.label13.Text = "H:";
            // 
            // labelW
            // 
            this.labelW.AutoSize = true;
            this.labelW.Location = new System.Drawing.Point(398, 320);
            this.labelW.Name = "labelW";
            this.labelW.Size = new System.Drawing.Size(13, 13);
            this.labelW.TabIndex = 47;
            this.labelW.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(379, 320);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 13);
            this.label14.TabIndex = 46;
            this.label14.Text = "W:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(313, 320);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 13);
            this.label15.TabIndex = 44;
            this.label15.Text = "DpiY:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(265, 320);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 13);
            this.label16.TabIndex = 42;
            this.label16.Text = "DpiX:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 141);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(157, 13);
            this.label17.TabIndex = 41;
            this.label17.Text = "--------------------------------------------------";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 192);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 40;
            this.label18.Text = "DPI Y";
            // 
            // textBoxDpiY
            // 
            this.textBoxDpiY.Location = new System.Drawing.Point(55, 187);
            this.textBoxDpiY.Name = "textBoxDpiY";
            this.textBoxDpiY.Size = new System.Drawing.Size(100, 20);
            this.textBoxDpiY.TabIndex = 39;
            this.textBoxDpiY.Text = "300";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(14, 166);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "DPI X";
            // 
            // textBoxDpiX
            // 
            this.textBoxDpiX.Location = new System.Drawing.Point(55, 161);
            this.textBoxDpiX.Name = "textBoxDpiX";
            this.textBoxDpiX.Size = new System.Drawing.Size(100, 20);
            this.textBoxDpiX.TabIndex = 37;
            this.textBoxDpiX.Text = "300";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 116);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 13);
            this.label20.TabIndex = 36;
            this.label20.Text = "Height";
            // 
            // textBoxHeight
            // 
            this.textBoxHeight.Location = new System.Drawing.Point(55, 112);
            this.textBoxHeight.Name = "textBoxHeight";
            this.textBoxHeight.Size = new System.Drawing.Size(100, 20);
            this.textBoxHeight.TabIndex = 35;
            this.textBoxHeight.Text = "4800";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(14, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Width";
            // 
            // textBoxWidth
            // 
            this.textBoxWidth.Location = new System.Drawing.Point(55, 86);
            this.textBoxWidth.Name = "textBoxWidth";
            this.textBoxWidth.Size = new System.Drawing.Size(100, 20);
            this.textBoxWidth.TabIndex = 33;
            this.textBoxWidth.Text = "3600";
            // 
            // buttonExport
            // 
            this.buttonExport.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.buttonExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExport.ForeColor = System.Drawing.Color.White;
            this.buttonExport.Location = new System.Drawing.Point(17, 296);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(100, 44);
            this.buttonExport.TabIndex = 31;
            this.buttonExport.Text = "Xuất Hình";
            this.buttonExport.UseVisualStyleBackColor = false;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // labelProcessFile
            // 
            this.labelProcessFile.AutoSize = true;
            this.labelProcessFile.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProcessFile.ForeColor = System.Drawing.Color.LimeGreen;
            this.labelProcessFile.Location = new System.Drawing.Point(407, 47);
            this.labelProcessFile.Name = "labelProcessFile";
            this.labelProcessFile.Size = new System.Drawing.Size(23, 23);
            this.labelProcessFile.TabIndex = 30;
            this.labelProcessFile.Text = "0";
            // 
            // labelTotalFile
            // 
            this.labelTotalFile.AutoSize = true;
            this.labelTotalFile.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalFile.ForeColor = System.Drawing.Color.OrangeRed;
            this.labelTotalFile.Location = new System.Drawing.Point(407, 16);
            this.labelTotalFile.Name = "labelTotalFile";
            this.labelTotalFile.Size = new System.Drawing.Size(23, 23);
            this.labelTotalFile.TabIndex = 29;
            this.labelTotalFile.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "Folder path";
            // 
            // textBoxFolderTeescape
            // 
            this.textBoxFolderTeescape.Location = new System.Drawing.Point(82, 22);
            this.textBoxFolderTeescape.Name = "textBoxFolderTeescape";
            this.textBoxFolderTeescape.Size = new System.Drawing.Size(212, 20);
            this.textBoxFolderTeescape.TabIndex = 26;
            this.textBoxFolderTeescape.TextChanged += new System.EventHandler(this.textBoxFolderTeescape_TextChanged);
            this.textBoxFolderTeescape.DoubleClick += new System.EventHandler(this.textBoxFolderTeescape_DoubleClick);
            // 
            // tabPageResizeTool
            // 
            this.tabPageResizeTool.BackColor = System.Drawing.Color.Silver;
            this.tabPageResizeTool.Controls.Add(this.textBoxOutputFolder);
            this.tabPageResizeTool.Controls.Add(this.checkBoxHoodie);
            this.tabPageResizeTool.Controls.Add(this.label1);
            this.tabPageResizeTool.Controls.Add(this.labelTotalOut);
            this.tabPageResizeTool.Controls.Add(this.textBoxInputFolder);
            this.tabPageResizeTool.Controls.Add(this.labelTotalInput);
            this.tabPageResizeTool.Controls.Add(this.buttonStart);
            this.tabPageResizeTool.Controls.Add(this.checkBoxClearFolder);
            this.tabPageResizeTool.Controls.Add(this.label2);
            this.tabPageResizeTool.Controls.Add(this.label4);
            this.tabPageResizeTool.Controls.Add(this.label3);
            this.tabPageResizeTool.Controls.Add(this.textBoxH);
            this.tabPageResizeTool.Controls.Add(this.textBoxW);
            this.tabPageResizeTool.Location = new System.Drawing.Point(4, 22);
            this.tabPageResizeTool.Name = "tabPageResizeTool";
            this.tabPageResizeTool.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageResizeTool.Size = new System.Drawing.Size(562, 383);
            this.tabPageResizeTool.TabIndex = 0;
            this.tabPageResizeTool.Text = "Merch Resizing";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Silver;
            this.tabPage2.Controls.Add(this.buttonCleanDuplicatedFile);
            this.tabPage2.Controls.Add(this.buttonReplaceName);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.textBoxMinFile);
            this.tabPage2.Controls.Add(this.buttonStartClean);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.textBoxInputCleanDirectory);
            this.tabPage2.Controls.Add(this.labelTotalDeleted);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(562, 383);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Clean Directory";
            // 
            // buttonReplaceName
            // 
            this.buttonReplaceName.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonReplaceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReplaceName.ForeColor = System.Drawing.Color.White;
            this.buttonReplaceName.Location = new System.Drawing.Point(76, 236);
            this.buttonReplaceName.Name = "buttonReplaceName";
            this.buttonReplaceName.Size = new System.Drawing.Size(150, 49);
            this.buttonReplaceName.TabIndex = 21;
            this.buttonReplaceName.Text = "Start Replace";
            this.buttonReplaceName.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Min total file:";
            // 
            // textBoxMinFile
            // 
            this.textBoxMinFile.Location = new System.Drawing.Point(76, 49);
            this.textBoxMinFile.Name = "textBoxMinFile";
            this.textBoxMinFile.Size = new System.Drawing.Size(34, 20);
            this.textBoxMinFile.TabIndex = 16;
            this.textBoxMinFile.Text = "1";
            // 
            // buttonStartClean
            // 
            this.buttonStartClean.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonStartClean.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartClean.ForeColor = System.Drawing.Color.White;
            this.buttonStartClean.Location = new System.Drawing.Point(73, 75);
            this.buttonStartClean.Name = "buttonStartClean";
            this.buttonStartClean.Size = new System.Drawing.Size(153, 49);
            this.buttonStartClean.TabIndex = 14;
            this.buttonStartClean.Text = "Start Clean < min file";
            this.buttonStartClean.UseVisualStyleBackColor = false;
            this.buttonStartClean.Click += new System.EventHandler(this.buttonStartClean_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Input folder:";
            // 
            // textBoxInputCleanDirectory
            // 
            this.textBoxInputCleanDirectory.Location = new System.Drawing.Point(76, 17);
            this.textBoxInputCleanDirectory.Name = "textBoxInputCleanDirectory";
            this.textBoxInputCleanDirectory.Size = new System.Drawing.Size(270, 20);
            this.textBoxInputCleanDirectory.TabIndex = 12;
            this.textBoxInputCleanDirectory.TextChanged += new System.EventHandler(this.textBoxInputCleanDirectory_TextChanged);
            // 
            // labelTotalDeleted
            // 
            this.labelTotalDeleted.AutoSize = true;
            this.labelTotalDeleted.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalDeleted.ForeColor = System.Drawing.Color.OrangeRed;
            this.labelTotalDeleted.Location = new System.Drawing.Point(376, 15);
            this.labelTotalDeleted.Name = "labelTotalDeleted";
            this.labelTotalDeleted.Size = new System.Drawing.Size(52, 54);
            this.labelTotalDeleted.TabIndex = 13;
            this.labelTotalDeleted.Text = "0";
            // 
            // tabPageGoogleDrive
            // 
            this.tabPageGoogleDrive.BackColor = System.Drawing.Color.Silver;
            this.tabPageGoogleDrive.Controls.Add(this.checkBoxFirebaseTracking);
            this.tabPageGoogleDrive.Controls.Add(this.checkBoxMergeAfterDone);
            this.tabPageGoogleDrive.Controls.Add(this.textBoxReplaceBy);
            this.tabPageGoogleDrive.Controls.Add(this.label35);
            this.tabPageGoogleDrive.Controls.Add(this.label34);
            this.tabPageGoogleDrive.Controls.Add(this.textBoxFindText);
            this.tabPageGoogleDrive.Controls.Add(this.label33);
            this.tabPageGoogleDrive.Controls.Add(this.textBoxTrackingFilePath);
            this.tabPageGoogleDrive.Controls.Add(this.label32);
            this.tabPageGoogleDrive.Controls.Add(this.textBoxOutputDriveRename);
            this.tabPageGoogleDrive.Controls.Add(this.label31);
            this.tabPageGoogleDrive.Controls.Add(this.textBoxParentFolderDrive);
            this.tabPageGoogleDrive.Controls.Add(this.buttonRenameDrive);
            this.tabPageGoogleDrive.Controls.Add(this.labelDownloaded);
            this.tabPageGoogleDrive.Controls.Add(this.label9);
            this.tabPageGoogleDrive.Controls.Add(this.textBoxDownloadTo);
            this.tabPageGoogleDrive.Controls.Add(this.label6);
            this.tabPageGoogleDrive.Controls.Add(this.textBoxAppName);
            this.tabPageGoogleDrive.Controls.Add(this.checkBoxDeleteAfterDown);
            this.tabPageGoogleDrive.Controls.Add(this.buttonDriveDownload);
            this.tabPageGoogleDrive.Controls.Add(this.label8);
            this.tabPageGoogleDrive.Controls.Add(this.textBoxDriveGuid);
            this.tabPageGoogleDrive.Location = new System.Drawing.Point(4, 22);
            this.tabPageGoogleDrive.Name = "tabPageGoogleDrive";
            this.tabPageGoogleDrive.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGoogleDrive.Size = new System.Drawing.Size(562, 383);
            this.tabPageGoogleDrive.TabIndex = 2;
            this.tabPageGoogleDrive.Text = "Drive downloader";
            // 
            // checkBoxFirebaseTracking
            // 
            this.checkBoxFirebaseTracking.AutoSize = true;
            this.checkBoxFirebaseTracking.Location = new System.Drawing.Point(355, 278);
            this.checkBoxFirebaseTracking.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxFirebaseTracking.Name = "checkBoxFirebaseTracking";
            this.checkBoxFirebaseTracking.Size = new System.Drawing.Size(88, 17);
            this.checkBoxFirebaseTracking.TabIndex = 38;
            this.checkBoxFirebaseTracking.Text = "Use Firebase";
            this.checkBoxFirebaseTracking.UseVisualStyleBackColor = true;
            // 
            // checkBoxMergeAfterDone
            // 
            this.checkBoxMergeAfterDone.AutoSize = true;
            this.checkBoxMergeAfterDone.Checked = true;
            this.checkBoxMergeAfterDone.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMergeAfterDone.Location = new System.Drawing.Point(355, 304);
            this.checkBoxMergeAfterDone.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxMergeAfterDone.Name = "checkBoxMergeAfterDone";
            this.checkBoxMergeAfterDone.Size = new System.Drawing.Size(109, 17);
            this.checkBoxMergeAfterDone.TabIndex = 37;
            this.checkBoxMergeAfterDone.Text = "Merge after Done";
            this.checkBoxMergeAfterDone.UseVisualStyleBackColor = true;
            // 
            // textBoxReplaceBy
            // 
            this.textBoxReplaceBy.Location = new System.Drawing.Point(243, 255);
            this.textBoxReplaceBy.Name = "textBoxReplaceBy";
            this.textBoxReplaceBy.Size = new System.Drawing.Size(114, 20);
            this.textBoxReplaceBy.TabIndex = 36;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(185, 257);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(61, 13);
            this.label35.TabIndex = 35;
            this.label35.Text = "Replace by";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(32, 259);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(47, 13);
            this.label34.TabIndex = 33;
            this.label34.Text = "Find text";
            // 
            // textBoxFindText
            // 
            this.textBoxFindText.Location = new System.Drawing.Point(85, 256);
            this.textBoxFindText.Name = "textBoxFindText";
            this.textBoxFindText.Size = new System.Drawing.Size(86, 20);
            this.textBoxFindText.TabIndex = 34;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(17, 234);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(65, 13);
            this.label33.TabIndex = 31;
            this.label33.Text = "Tracking file";
            // 
            // textBoxTrackingFilePath
            // 
            this.textBoxTrackingFilePath.Location = new System.Drawing.Point(86, 232);
            this.textBoxTrackingFilePath.Name = "textBoxTrackingFilePath";
            this.textBoxTrackingFilePath.Size = new System.Drawing.Size(270, 20);
            this.textBoxTrackingFilePath.TabIndex = 32;
            this.textBoxTrackingFilePath.Text = "C:\\Users\\Cuong\\Downloads\\Mixmockup\\tracking.txt";
            this.textBoxTrackingFilePath.DoubleClick += new System.EventHandler(this.textBoxTrackingFilePath_DoubleClick);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(17, 210);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 13);
            this.label32.TabIndex = 29;
            this.label32.Text = "OutFolder";
            // 
            // textBoxOutputDriveRename
            // 
            this.textBoxOutputDriveRename.Location = new System.Drawing.Point(86, 207);
            this.textBoxOutputDriveRename.Name = "textBoxOutputDriveRename";
            this.textBoxOutputDriveRename.Size = new System.Drawing.Size(270, 20);
            this.textBoxOutputDriveRename.TabIndex = 30;
            this.textBoxOutputDriveRename.Text = "C:\\Users\\Cuong\\Downloads\\Mixmockup\\RawSync\\";
            this.textBoxOutputDriveRename.DoubleClick += new System.EventHandler(this.textBoxOutputDriveRename_DoubleClick);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 183);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(67, 13);
            this.label31.TabIndex = 27;
            this.label31.Text = "Parent folder";
            // 
            // textBoxParentFolderDrive
            // 
            this.textBoxParentFolderDrive.Location = new System.Drawing.Point(86, 180);
            this.textBoxParentFolderDrive.Name = "textBoxParentFolderDrive";
            this.textBoxParentFolderDrive.Size = new System.Drawing.Size(270, 20);
            this.textBoxParentFolderDrive.TabIndex = 28;
            this.textBoxParentFolderDrive.Text = "C:\\Users\\Cuong\\Google Drive\\Speed\\Teescape\\ForUpload\\";
            this.textBoxParentFolderDrive.TextChanged += new System.EventHandler(this.textBoxParentFolderDrive_TextChanged);
            this.textBoxParentFolderDrive.DoubleClick += new System.EventHandler(this.textBoxParentFolderDrive_DoubleClick);
            // 
            // buttonRenameDrive
            // 
            this.buttonRenameDrive.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonRenameDrive.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRenameDrive.ForeColor = System.Drawing.Color.White;
            this.buttonRenameDrive.Location = new System.Drawing.Point(83, 285);
            this.buttonRenameDrive.Name = "buttonRenameDrive";
            this.buttonRenameDrive.Size = new System.Drawing.Size(226, 49);
            this.buttonRenameDrive.TabIndex = 26;
            this.buttonRenameDrive.Text = "Rename and Tracking";
            this.buttonRenameDrive.UseVisualStyleBackColor = false;
            this.buttonRenameDrive.Click += new System.EventHandler(this.buttonRenameDrive_Click);
            // 
            // labelDownloaded
            // 
            this.labelDownloaded.AutoSize = true;
            this.labelDownloaded.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDownloaded.ForeColor = System.Drawing.Color.OrangeRed;
            this.labelDownloaded.Location = new System.Drawing.Point(375, 21);
            this.labelDownloaded.Name = "labelDownloaded";
            this.labelDownloaded.Size = new System.Drawing.Size(52, 54);
            this.labelDownloaded.TabIndex = 25;
            this.labelDownloaded.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Download to";
            // 
            // textBoxDownloadTo
            // 
            this.textBoxDownloadTo.Location = new System.Drawing.Point(86, 74);
            this.textBoxDownloadTo.Name = "textBoxDownloadTo";
            this.textBoxDownloadTo.Size = new System.Drawing.Size(270, 20);
            this.textBoxDownloadTo.TabIndex = 24;
            this.textBoxDownloadTo.Text = "C:\\GoogleDrive";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "App Name:";
            // 
            // textBoxAppName
            // 
            this.textBoxAppName.Location = new System.Drawing.Point(85, 47);
            this.textBoxAppName.Name = "textBoxAppName";
            this.textBoxAppName.Size = new System.Drawing.Size(270, 20);
            this.textBoxAppName.TabIndex = 22;
            this.textBoxAppName.Text = "merch";
            // 
            // checkBoxDeleteAfterDown
            // 
            this.checkBoxDeleteAfterDown.AutoSize = true;
            this.checkBoxDeleteAfterDown.Location = new System.Drawing.Point(85, 102);
            this.checkBoxDeleteAfterDown.Name = "checkBoxDeleteAfterDown";
            this.checkBoxDeleteAfterDown.Size = new System.Drawing.Size(130, 17);
            this.checkBoxDeleteAfterDown.TabIndex = 20;
            this.checkBoxDeleteAfterDown.Text = "Delete after download";
            this.checkBoxDeleteAfterDown.UseVisualStyleBackColor = true;
            // 
            // buttonDriveDownload
            // 
            this.buttonDriveDownload.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonDriveDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDriveDownload.ForeColor = System.Drawing.Color.White;
            this.buttonDriveDownload.Location = new System.Drawing.Point(88, 126);
            this.buttonDriveDownload.Name = "buttonDriveDownload";
            this.buttonDriveDownload.Size = new System.Drawing.Size(82, 47);
            this.buttonDriveDownload.TabIndex = 19;
            this.buttonDriveDownload.Text = "Start";
            this.buttonDriveDownload.UseVisualStyleBackColor = false;
            this.buttonDriveDownload.Click += new System.EventHandler(this.buttonDriveDownload_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Folder guid:";
            // 
            // textBoxDriveGuid
            // 
            this.textBoxDriveGuid.Location = new System.Drawing.Point(85, 16);
            this.textBoxDriveGuid.Name = "textBoxDriveGuid";
            this.textBoxDriveGuid.Size = new System.Drawing.Size(270, 20);
            this.textBoxDriveGuid.TabIndex = 18;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.textboxResult);
            this.tabPage1.Controls.Add(this.textBoxConvertResult);
            this.tabPage1.Controls.Add(this.buttonStartConvert);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.textBoxDateInput);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(562, 383);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Converter";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 205);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(310, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "(Note: Find in shopify product name prefix, create date product..)";
            // 
            // textboxResult
            // 
            this.textboxResult.AutoSize = true;
            this.textboxResult.Location = new System.Drawing.Point(7, 48);
            this.textboxResult.Name = "textboxResult";
            this.textboxResult.Size = new System.Drawing.Size(37, 13);
            this.textboxResult.TabIndex = 26;
            this.textboxResult.Text = "Result";
            // 
            // textBoxConvertResult
            // 
            this.textBoxConvertResult.Location = new System.Drawing.Point(76, 44);
            this.textBoxConvertResult.Name = "textBoxConvertResult";
            this.textBoxConvertResult.Size = new System.Drawing.Size(270, 20);
            this.textBoxConvertResult.TabIndex = 27;
            // 
            // buttonStartConvert
            // 
            this.buttonStartConvert.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonStartConvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartConvert.ForeColor = System.Drawing.Color.White;
            this.buttonStartConvert.Location = new System.Drawing.Point(74, 125);
            this.buttonStartConvert.Name = "buttonStartConvert";
            this.buttonStartConvert.Size = new System.Drawing.Size(82, 47);
            this.buttonStartConvert.TabIndex = 25;
            this.buttonStartConvert.Text = "Start";
            this.buttonStartConvert.UseVisualStyleBackColor = false;
            this.buttonStartConvert.Click += new System.EventHandler(this.buttonStartConvert_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Date time";
            // 
            // textBoxDateInput
            // 
            this.textBoxDateInput.Location = new System.Drawing.Point(76, 15);
            this.textBoxDateInput.Name = "textBoxDateInput";
            this.textBoxDateInput.Size = new System.Drawing.Size(270, 20);
            this.textBoxDateInput.TabIndex = 24;
            // 
            // googleFeedTab
            // 
            this.googleFeedTab.Controls.Add(this.checkBoxIsBingAds);
            this.googleFeedTab.Controls.Add(this.labelCounter);
            this.googleFeedTab.Controls.Add(this.label30);
            this.googleFeedTab.Controls.Add(this.textBoxRange);
            this.googleFeedTab.Controls.Add(this.label29);
            this.googleFeedTab.Controls.Add(this.textBoxDBName);
            this.googleFeedTab.Controls.Add(this.label28);
            this.googleFeedTab.Controls.Add(this.textBoxInterval);
            this.googleFeedTab.Controls.Add(this.label27);
            this.googleFeedTab.Controls.Add(this.textBoxSiteAddress);
            this.googleFeedTab.Controls.Add(this.label26);
            this.googleFeedTab.Controls.Add(this.textBoxDBPass);
            this.googleFeedTab.Controls.Add(this.label25);
            this.googleFeedTab.Controls.Add(this.textBoxDBUser);
            this.googleFeedTab.Controls.Add(this.label24);
            this.googleFeedTab.Controls.Add(this.textBoxDBIP);
            this.googleFeedTab.Controls.Add(this.label23);
            this.googleFeedTab.Controls.Add(this.textBoxSheetId);
            this.googleFeedTab.Controls.Add(this.buttonStartFeed);
            this.googleFeedTab.Location = new System.Drawing.Point(4, 22);
            this.googleFeedTab.Name = "googleFeedTab";
            this.googleFeedTab.Padding = new System.Windows.Forms.Padding(3);
            this.googleFeedTab.Size = new System.Drawing.Size(562, 383);
            this.googleFeedTab.TabIndex = 5;
            this.googleFeedTab.Text = "Google/Bing Feed";
            this.googleFeedTab.UseVisualStyleBackColor = true;
            // 
            // checkBoxIsBingAds
            // 
            this.checkBoxIsBingAds.AutoSize = true;
            this.checkBoxIsBingAds.Location = new System.Drawing.Point(409, 34);
            this.checkBoxIsBingAds.Margin = new System.Windows.Forms.Padding(1);
            this.checkBoxIsBingAds.Name = "checkBoxIsBingAds";
            this.checkBoxIsBingAds.Size = new System.Drawing.Size(79, 17);
            this.checkBoxIsBingAds.TabIndex = 36;
            this.checkBoxIsBingAds.Text = "Is Bing Ads";
            this.checkBoxIsBingAds.UseVisualStyleBackColor = true;
            // 
            // labelCounter
            // 
            this.labelCounter.AutoSize = true;
            this.labelCounter.Font = new System.Drawing.Font("Courier New", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCounter.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelCounter.Location = new System.Drawing.Point(181, 291);
            this.labelCounter.Name = "labelCounter";
            this.labelCounter.Size = new System.Drawing.Size(75, 24);
            this.labelCounter.TabIndex = 35;
            this.labelCounter.Text = "--:--";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(356, 278);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(39, 13);
            this.label30.TabIndex = 34;
            this.label30.Text = "Range";
            // 
            // textBoxRange
            // 
            this.textBoxRange.Location = new System.Drawing.Point(404, 275);
            this.textBoxRange.Name = "textBoxRange";
            this.textBoxRange.Size = new System.Drawing.Size(66, 20);
            this.textBoxRange.TabIndex = 33;
            this.textBoxRange.Text = "100";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(18, 181);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 13);
            this.label29.TabIndex = 31;
            this.label29.Text = "DB Name";
            // 
            // textBoxDBName
            // 
            this.textBoxDBName.Location = new System.Drawing.Point(89, 178);
            this.textBoxDBName.Name = "textBoxDBName";
            this.textBoxDBName.PasswordChar = '*';
            this.textBoxDBName.Size = new System.Drawing.Size(270, 20);
            this.textBoxDBName.TabIndex = 32;
            this.textBoxDBName.Text = "rocketstack";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(356, 314);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(42, 13);
            this.label28.TabIndex = 30;
            this.label28.Text = "Interval";
            // 
            // textBoxInterval
            // 
            this.textBoxInterval.Location = new System.Drawing.Point(404, 310);
            this.textBoxInterval.Name = "textBoxInterval";
            this.textBoxInterval.Size = new System.Drawing.Size(66, 20);
            this.textBoxInterval.TabIndex = 29;
            this.textBoxInterval.Text = "20";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(17, 212);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(65, 13);
            this.label27.TabIndex = 27;
            this.label27.Text = "Site address";
            // 
            // textBoxSiteAddress
            // 
            this.textBoxSiteAddress.Location = new System.Drawing.Point(89, 209);
            this.textBoxSiteAddress.Name = "textBoxSiteAddress";
            this.textBoxSiteAddress.Size = new System.Drawing.Size(270, 20);
            this.textBoxSiteAddress.TabIndex = 28;
            this.textBoxSiteAddress.Text = "https://tee4stars.com";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(18, 151);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(48, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "DB Pass";
            // 
            // textBoxDBPass
            // 
            this.textBoxDBPass.Location = new System.Drawing.Point(89, 148);
            this.textBoxDBPass.Name = "textBoxDBPass";
            this.textBoxDBPass.PasswordChar = '*';
            this.textBoxDBPass.Size = new System.Drawing.Size(270, 20);
            this.textBoxDBPass.TabIndex = 26;
            this.textBoxDBPass.Text = "J#pfBeDcxHFDMnxrWU(3vCOh";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(20, 114);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 13);
            this.label25.TabIndex = 23;
            this.label25.Text = "DB User";
            // 
            // textBoxDBUser
            // 
            this.textBoxDBUser.Location = new System.Drawing.Point(90, 112);
            this.textBoxDBUser.Name = "textBoxDBUser";
            this.textBoxDBUser.Size = new System.Drawing.Size(270, 20);
            this.textBoxDBUser.TabIndex = 24;
            this.textBoxDBUser.Text = "root";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(20, 80);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 13);
            this.label24.TabIndex = 21;
            this.label24.Text = "DB Host IP";
            // 
            // textBoxDBIP
            // 
            this.textBoxDBIP.Location = new System.Drawing.Point(91, 77);
            this.textBoxDBIP.Name = "textBoxDBIP";
            this.textBoxDBIP.Size = new System.Drawing.Size(270, 20);
            this.textBoxDBIP.TabIndex = 22;
            this.textBoxDBIP.Text = "104.225.216.4";
            this.textBoxDBIP.TextChanged += new System.EventHandler(this.textBoxDBIP_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 36);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(81, 13);
            this.label23.TabIndex = 19;
            this.label23.Text = "Google SheetId";
            // 
            // textBoxSheetId
            // 
            this.textBoxSheetId.Location = new System.Drawing.Point(93, 34);
            this.textBoxSheetId.Name = "textBoxSheetId";
            this.textBoxSheetId.Size = new System.Drawing.Size(270, 20);
            this.textBoxSheetId.TabIndex = 20;
            this.textBoxSheetId.Text = "1SQvyNLyBHTMiu1fZKMA6Oh52g39l70AwzvD9v7MaLHY";
            // 
            // buttonStartFeed
            // 
            this.buttonStartFeed.BackColor = System.Drawing.Color.ForestGreen;
            this.buttonStartFeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartFeed.ForeColor = System.Drawing.Color.White;
            this.buttonStartFeed.Location = new System.Drawing.Point(79, 275);
            this.buttonStartFeed.Name = "buttonStartFeed";
            this.buttonStartFeed.Size = new System.Drawing.Size(75, 52);
            this.buttonStartFeed.TabIndex = 0;
            this.buttonStartFeed.Text = "Start";
            this.buttonStartFeed.UseVisualStyleBackColor = false;
            this.buttonStartFeed.Click += new System.EventHandler(this.buttonStartFeed_Click);
            // 
            // tabPageCSVImport
            // 
            this.tabPageCSVImport.Controls.Add(this.labelTotalCSVDone);
            this.tabPageCSVImport.Controls.Add(this.labelTotalCSV);
            this.tabPageCSVImport.Controls.Add(this.checkBoxIsFullPathExport);
            this.tabPageCSVImport.Controls.Add(this.label41);
            this.tabPageCSVImport.Controls.Add(this.textBoxImportCsvEndpoint);
            this.tabPageCSVImport.Controls.Add(this.label40);
            this.tabPageCSVImport.Controls.Add(this.textBoxCsvUploadFolderPath);
            this.tabPageCSVImport.Controls.Add(this.label37);
            this.tabPageCSVImport.Controls.Add(this.textBoxCSVPass);
            this.tabPageCSVImport.Controls.Add(this.label38);
            this.tabPageCSVImport.Controls.Add(this.textBoxCSVUser);
            this.tabPageCSVImport.Controls.Add(this.label39);
            this.tabPageCSVImport.Controls.Add(this.textBoxDBIP_csv);
            this.tabPageCSVImport.Controls.Add(this.buttonImportCSV);
            this.tabPageCSVImport.Location = new System.Drawing.Point(4, 22);
            this.tabPageCSVImport.Margin = new System.Windows.Forms.Padding(2);
            this.tabPageCSVImport.Name = "tabPageCSVImport";
            this.tabPageCSVImport.Padding = new System.Windows.Forms.Padding(2);
            this.tabPageCSVImport.Size = new System.Drawing.Size(562, 383);
            this.tabPageCSVImport.TabIndex = 6;
            this.tabPageCSVImport.Text = "CSV Import";
            this.tabPageCSVImport.UseVisualStyleBackColor = true;
            // 
            // labelTotalCSVDone
            // 
            this.labelTotalCSVDone.AutoSize = true;
            this.labelTotalCSVDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalCSVDone.Location = new System.Drawing.Point(338, 281);
            this.labelTotalCSVDone.Name = "labelTotalCSVDone";
            this.labelTotalCSVDone.Size = new System.Drawing.Size(36, 39);
            this.labelTotalCSVDone.TabIndex = 39;
            this.labelTotalCSVDone.Text = "0";
            // 
            // labelTotalCSV
            // 
            this.labelTotalCSV.AutoSize = true;
            this.labelTotalCSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalCSV.Location = new System.Drawing.Point(216, 281);
            this.labelTotalCSV.Name = "labelTotalCSV";
            this.labelTotalCSV.Size = new System.Drawing.Size(36, 39);
            this.labelTotalCSV.TabIndex = 38;
            this.labelTotalCSV.Text = "0";
            // 
            // checkBoxIsFullPathExport
            // 
            this.checkBoxIsFullPathExport.AutoSize = true;
            this.checkBoxIsFullPathExport.Location = new System.Drawing.Point(440, 28);
            this.checkBoxIsFullPathExport.Margin = new System.Windows.Forms.Padding(1);
            this.checkBoxIsFullPathExport.Name = "checkBoxIsFullPathExport";
            this.checkBoxIsFullPathExport.Size = new System.Drawing.Size(72, 17);
            this.checkBoxIsFullPathExport.TabIndex = 37;
            this.checkBoxIsFullPathExport.Text = "Is fullPath";
            this.checkBoxIsFullPathExport.UseVisualStyleBackColor = true;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(17, 177);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(67, 13);
            this.label41.TabIndex = 35;
            this.label41.Text = "CsvEndpoint";
            // 
            // textBoxImportCsvEndpoint
            // 
            this.textBoxImportCsvEndpoint.Location = new System.Drawing.Point(103, 175);
            this.textBoxImportCsvEndpoint.Name = "textBoxImportCsvEndpoint";
            this.textBoxImportCsvEndpoint.Size = new System.Drawing.Size(442, 20);
            this.textBoxImportCsvEndpoint.TabIndex = 36;
            this.textBoxImportCsvEndpoint.Text = "https://tee4stars.com/wp-content/plugins/gearment-import-Flatsome-v1.9.4/admin/te" +
    "st.php";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(17, 140);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(79, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "WP uploads dir";
            // 
            // textBoxCsvUploadFolderPath
            // 
            this.textBoxCsvUploadFolderPath.Location = new System.Drawing.Point(103, 138);
            this.textBoxCsvUploadFolderPath.Name = "textBoxCsvUploadFolderPath";
            this.textBoxCsvUploadFolderPath.Size = new System.Drawing.Size(270, 20);
            this.textBoxCsvUploadFolderPath.TabIndex = 34;
            this.textBoxCsvUploadFolderPath.Text = "/var/www/html/wp-content/uploads";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(16, 99);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 13);
            this.label37.TabIndex = 31;
            this.label37.Text = "Password";
            // 
            // textBoxCSVPass
            // 
            this.textBoxCSVPass.Location = new System.Drawing.Point(104, 96);
            this.textBoxCSVPass.Name = "textBoxCSVPass";
            this.textBoxCSVPass.PasswordChar = '*';
            this.textBoxCSVPass.Size = new System.Drawing.Size(270, 20);
            this.textBoxCSVPass.TabIndex = 32;
            this.textBoxCSVPass.Text = "J#pfBeDcxHFDMnxrWU(3vCOh";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(18, 62);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 13);
            this.label38.TabIndex = 29;
            this.label38.Text = "User";
            // 
            // textBoxCSVUser
            // 
            this.textBoxCSVUser.Location = new System.Drawing.Point(105, 60);
            this.textBoxCSVUser.Name = "textBoxCSVUser";
            this.textBoxCSVUser.Size = new System.Drawing.Size(270, 20);
            this.textBoxCSVUser.TabIndex = 30;
            this.textBoxCSVUser.Text = "root";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(18, 28);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(60, 13);
            this.label39.TabIndex = 27;
            this.label39.Text = "DB Host IP";
            // 
            // textBoxDBIP_csv
            // 
            this.textBoxDBIP_csv.Location = new System.Drawing.Point(105, 25);
            this.textBoxDBIP_csv.Name = "textBoxDBIP_csv";
            this.textBoxDBIP_csv.Size = new System.Drawing.Size(270, 20);
            this.textBoxDBIP_csv.TabIndex = 28;
            this.textBoxDBIP_csv.Text = "104.225.216.4";
            this.textBoxDBIP_csv.TextChanged += new System.EventHandler(this.textBoxDBIP_csv_TextChanged);
            // 
            // buttonImportCSV
            // 
            this.buttonImportCSV.BackColor = System.Drawing.Color.ForestGreen;
            this.buttonImportCSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImportCSV.ForeColor = System.Drawing.Color.White;
            this.buttonImportCSV.Location = new System.Drawing.Point(87, 281);
            this.buttonImportCSV.Margin = new System.Windows.Forms.Padding(2);
            this.buttonImportCSV.Name = "buttonImportCSV";
            this.buttonImportCSV.Size = new System.Drawing.Size(81, 46);
            this.buttonImportCSV.TabIndex = 0;
            this.buttonImportCSV.Text = "Import";
            this.buttonImportCSV.UseVisualStyleBackColor = false;
            this.buttonImportCSV.Click += new System.EventHandler(this.buttonImportCSV_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1200000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // flowLayoutPanelButton
            // 
            this.flowLayoutPanelButton.Controls.Add(this.label36);
            this.flowLayoutPanelButton.Controls.Add(this.comboBoxProfile);
            this.flowLayoutPanelButton.Controls.Add(this.buttonSaveProfile);
            this.flowLayoutPanelButton.Controls.Add(this.buttonRemoveProfile);
            this.flowLayoutPanelButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanelButton.Location = new System.Drawing.Point(0, 368);
            this.flowLayoutPanelButton.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanelButton.Name = "flowLayoutPanelButton";
            this.flowLayoutPanelButton.Size = new System.Drawing.Size(570, 41);
            this.flowLayoutPanelButton.TabIndex = 56;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(2, 0);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(50, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "  Profiles:";
            // 
            // comboBoxProfile
            // 
            this.comboBoxProfile.FormattingEnabled = true;
            this.comboBoxProfile.Location = new System.Drawing.Point(56, 2);
            this.comboBoxProfile.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxProfile.Name = "comboBoxProfile";
            this.comboBoxProfile.Size = new System.Drawing.Size(174, 21);
            this.comboBoxProfile.TabIndex = 1;
            this.comboBoxProfile.SelectedIndexChanged += new System.EventHandler(this.comboBoxProfile_SelectedIndexChanged);
            // 
            // buttonSaveProfile
            // 
            this.buttonSaveProfile.Location = new System.Drawing.Point(234, 2);
            this.buttonSaveProfile.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSaveProfile.Name = "buttonSaveProfile";
            this.buttonSaveProfile.Size = new System.Drawing.Size(61, 25);
            this.buttonSaveProfile.TabIndex = 2;
            this.buttonSaveProfile.Text = "Save";
            this.buttonSaveProfile.UseVisualStyleBackColor = true;
            this.buttonSaveProfile.Click += new System.EventHandler(this.buttonSaveProfile_Click);
            // 
            // buttonRemoveProfile
            // 
            this.buttonRemoveProfile.Location = new System.Drawing.Point(299, 2);
            this.buttonRemoveProfile.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRemoveProfile.Name = "buttonRemoveProfile";
            this.buttonRemoveProfile.Size = new System.Drawing.Size(61, 25);
            this.buttonRemoveProfile.TabIndex = 3;
            this.buttonRemoveProfile.Text = "Delete";
            this.buttonRemoveProfile.UseVisualStyleBackColor = true;
            this.buttonRemoveProfile.Click += new System.EventHandler(this.buttonRemoveProfile_Click);
            // 
            // buttonCleanDuplicatedFile
            // 
            this.buttonCleanDuplicatedFile.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonCleanDuplicatedFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCleanDuplicatedFile.ForeColor = System.Drawing.Color.White;
            this.buttonCleanDuplicatedFile.Location = new System.Drawing.Point(241, 75);
            this.buttonCleanDuplicatedFile.Name = "buttonCleanDuplicatedFile";
            this.buttonCleanDuplicatedFile.Size = new System.Drawing.Size(153, 49);
            this.buttonCleanDuplicatedFile.TabIndex = 22;
            this.buttonCleanDuplicatedFile.Text = "Clean duplicated file";
            this.buttonCleanDuplicatedFile.UseVisualStyleBackColor = false;
            this.buttonCleanDuplicatedFile.Click += new System.EventHandler(this.buttonCleanDuplicatedFile_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 409);
            this.Controls.Add(this.flowLayoutPanelButton);
            this.Controls.Add(this.tabControlResizeTool);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "The Utilities Applciation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControlResizeTool.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDriveDone)).EndInit();
            this.tabPageResizeTool.ResumeLayout(false);
            this.tabPageResizeTool.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPageGoogleDrive.ResumeLayout(false);
            this.tabPageGoogleDrive.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.googleFeedTab.ResumeLayout(false);
            this.googleFeedTab.PerformLayout();
            this.tabPageCSVImport.ResumeLayout(false);
            this.tabPageCSVImport.PerformLayout();
            this.flowLayoutPanelButton.ResumeLayout(false);
            this.flowLayoutPanelButton.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxInputFolder;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TextBox textBoxOutputFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxW;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxH;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxClearFolder;
        private System.Windows.Forms.Label labelTotalInput;
        private System.Windows.Forms.Label labelTotalOut;
        private System.Windows.Forms.CheckBox checkBoxHoodie;
        private System.Windows.Forms.TabControl tabControlResizeTool;
        private System.Windows.Forms.TabPage tabPageResizeTool;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxMinFile;
        private System.Windows.Forms.Button buttonStartClean;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxInputCleanDirectory;
        private System.Windows.Forms.Label labelTotalDeleted;
        private System.Windows.Forms.TabPage tabPageGoogleDrive;
        private System.Windows.Forms.CheckBox checkBoxDeleteAfterDown;
        private System.Windows.Forms.Button buttonDriveDownload;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxDriveGuid;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxAppName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxDownloadTo;
        private System.Windows.Forms.Label labelDownloaded;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label textboxResult;
        private System.Windows.Forms.TextBox textBoxConvertResult;
        private System.Windows.Forms.Button buttonStartConvert;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxDateInput;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label labelProcessFile;
        private System.Windows.Forms.Label labelTotalFile;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxFolderTeescape;
        private System.Windows.Forms.Label labelDpiX;
        private System.Windows.Forms.Label labelDpiY;
        private System.Windows.Forms.Label labelH;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelW;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxDpiY;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxDpiX;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxHeight;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxWidth;
        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.Button buttonFromClipboard;
        private System.Windows.Forms.Label labelCurrentImageName;
        private System.Windows.Forms.Button buttonDownloadAndExport;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.RichTextBox richTextBoxImageNames;
        private System.Windows.Forms.TabPage googleFeedTab;
        private System.Windows.Forms.Button buttonStartFeed;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxSiteAddress;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxDBPass;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxDBUser;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBoxDBIP;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxSheetId;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBoxInterval;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxDBName;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxRange;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelCounter;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxTrackingFilePath;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBoxOutputDriveRename;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxParentFolderDrive;
        private System.Windows.Forms.Button buttonRenameDrive;
        private System.Windows.Forms.Button buttonReplaceName;
        private System.Windows.Forms.TextBox textBoxReplaceBy;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBoxFindText;
        private System.Windows.Forms.CheckBox checkBoxMergeAfterDone;
        private System.Windows.Forms.CheckBox checkBoxFirebaseTracking;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelButton;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox comboBoxProfile;
        private System.Windows.Forms.Button buttonSaveProfile;
        private System.Windows.Forms.Button buttonRemoveProfile;
        private System.Windows.Forms.CheckBox checkBoxIsBingAds;
        private System.Windows.Forms.TabPage tabPageCSVImport;
        private System.Windows.Forms.Button buttonImportCSV;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBoxImportCsvEndpoint;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBoxCsvUploadFolderPath;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBoxCSVPass;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBoxCSVUser;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBoxDBIP_csv;
        private System.Windows.Forms.CheckBox checkBoxIsFullPathExport;
        private System.Windows.Forms.Label labelTotalCSVDone;
        private System.Windows.Forms.Label labelTotalCSV;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.DataGridView dataGridViewDriveDone;
        private System.Windows.Forms.DataGridViewButtonColumn GCopy;
        private System.Windows.Forms.CheckBox checkBoxSkipUploadDrive;
        private System.Windows.Forms.Button buttonCleanResizedDir;
        private System.Windows.Forms.Button buttonCleanDuplicatedFile;
    }
}

