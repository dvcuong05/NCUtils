﻿using Main.classes.merch;
using System;
using System.Collections.Generic;

namespace NCUtils.DataClass.merch
{
    public class AMZConfig
    {
        private String description = "";
        private String productKey1 = "";
        private String productKey2 = "";
        private Boolean isOnBack;
        private string brandName;
        private string status;
        private List<ShirtConfig> shirtConfigs = new List<ShirtConfig>();


        //private List<BaseSetting> supportedProductTypes = new List<BaseSetting>();
        //private List<BaseSetting> supportedColors = new List<BaseSetting>();
        //private List<BaseSetting> productTypes = new List<BaseSetting>();
        //private List<BaseSetting> fitTypes = new List<BaseSetting>();

        public string Description { get => description; set => description = value; }
        public string ProductKey1 { get => productKey1; set => productKey1 = value; }
        public string ProductKey2 { get => productKey2; set => productKey2 = value; }
        public bool IsOnBack { get => isOnBack; set => isOnBack = value; }
        public string BrandName { get => brandName; set => brandName = value; }
        public string Status { get => status; set => status = value; }
        public List<ShirtConfig> ShirtConfigs { get => shirtConfigs; set => shirtConfigs = value; }
    }
}
