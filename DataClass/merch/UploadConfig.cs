﻿using System;
using System.Collections.Generic;

namespace NCUtils.DataClass.merch
{
    public class UploadConfig
    {
        private String folderPath;
        private String mainKeyword;
        private string addKey;
        private String[] replaceKeywords;
        private AMZConfig amzConfig;
        private List<Main.ItemConfig> itemsConfig;
        private string hoodieFolderPath;
        private string namePrefix;
        private string nameSuffix;
        private Boolean isNameToKey1;
        private string hoodieCropType;

        public String FolderPath
        {
            get { return folderPath; }
            set { folderPath = value; }
        }

        
        public List<Main.ItemConfig> ItemsConfig
        {
            get { return itemsConfig; }
            set { itemsConfig = value; }
        }

        public string[] ReplaceKeywords
        {
            get
            {
                return replaceKeywords;
            }

            set
            {
                replaceKeywords = value;
            }
        }

        public string MainKeyword
        {
            get
            {
                return mainKeyword;
            }

            set
            {
                mainKeyword = value;
            }
        }

        public AMZConfig AmzConfig { get => amzConfig; set => amzConfig = value; }
        public string AddKey { get => addKey; set => addKey = value; }
        public string HoodieFolderPath { get => hoodieFolderPath; set => hoodieFolderPath = value; }
        public string NamePrefix { get => namePrefix; set => namePrefix = value; }
        public string NameSuffix { get => nameSuffix; set => nameSuffix = value; }
        public bool IsNameToKey1 { get => isNameToKey1; set => isNameToKey1 = value; }
        public string HoodieCropType { get => hoodieCropType; set => hoodieCropType = value; }
    }
}
