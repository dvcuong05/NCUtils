﻿using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DataClass
{
    class SFTPClient
    {
        public static void UploadSFTPFile(string host, string username,
                                string password, string sourcefile, 
                                string destinationpath, int port)
        {
            using (SftpClient client = new SftpClient(host, port, username, password))
            {
                int attempts = 0;
                do
                {
                    try
                    {
                        client.Connect();
                    }
                    catch (Renci.SshNet.Common.SshConnectionException e)
                    {
                        attempts++;
                    }
                } while (attempts < 10 && !client.IsConnected);

                if (!client.Exists(destinationpath))
                {
                    client.CreateDirectory(destinationpath);
                }
                client.ChangeDirectory(destinationpath);
                using (FileStream fs = new FileStream(sourcefile, FileMode.Open))
                {
                    client.BufferSize = 4 * 1024;
                    client.UploadFile(fs, Path.GetFileName(sourcefile));
                }
            }

        }

        public static void MoveSFTPFile(string host, string username,
                                string password,
                                string filePath, string newPath, string basePath, int port)
        {
            using (SftpClient client = new SftpClient(host, port, username, password))
            {

                int attempts = 0;
                do
                {
                    try
                    {
                        client.Connect();
                    }
                    catch (Renci.SshNet.Common.SshConnectionException e)
                    {
                        attempts++;
                    }
                } while (attempts < 10 && !client.IsConnected);
                if (!client.Exists(basePath))
                {
                    client.CreateDirectory(basePath);
                }
                SftpFile inFile = client.Get(filePath);
                inFile.MoveTo(newPath);
            }
        }

        public static List<string> ListFilesInDir(string host, string username,
                                string password, int port, string dirPath)
        {
            List<string> res = new List<string>();
            using (SftpClient client = new SftpClient(host, port, username, password))
            {

                int attempts = 0;
                do
                {
                    try
                    {
                        client.Connect();
                    }
                    catch (Renci.SshNet.Common.SshConnectionException e)
                    {
                        attempts++;
                    }
                } while (attempts < 10 && !client.IsConnected);
                if (!client.Exists(dirPath))
                {
                    throw new Exception("Folder not exist:" + dirPath);
                }

                System.Collections.Generic.IEnumerable<SftpFile> fileList = client.ListDirectory(dirPath);
                foreach (SftpFile fi in fileList)
                {
                    if (Path.GetExtension(fi.FullName) == ".csv")
                    {
                        res.Add(Path.GetFileName(fi.FullName));
                    }
                }
                return res;
            }
        }

        public static void deletedirectory(string host, string username,string password, int port, string basePath)
        {
            using (SftpClient client = new SftpClient(host, port, username, password))
            {

                int attempts = 0;
                do
                {
                    try
                    {
                        client.Connect();
                    }
                    catch (Renci.SshNet.Common.SshConnectionException e)
                    {
                        attempts++;
                    }
                } while (attempts < 10 && !client.IsConnected);
                if (client.Exists(basePath))
                {
                    client.DeleteDirectory(basePath);
                }
            }
        }

        public static void createDirectory(FTPConfig fTPConfig, String folderName)
        {
            using (SftpClient client = new SftpClient(fTPConfig.Host, fTPConfig.Port, fTPConfig.UserName, fTPConfig.Password))
            {
                int attempts = 0;
                do
                {
                    try
                    {
                        client.Connect();
                    }
                    catch (Renci.SshNet.Common.SshConnectionException e)
                    {
                        attempts++;
                    }
                } while (attempts < 10 && !client.IsConnected);
                String folderPath = fTPConfig.BaseFolder + (fTPConfig.BaseFolder.EndsWith("/") ? "" : "/") + folderName;
                if (!client.Exists(folderPath))
                {
                    client.CreateDirectory(folderPath);
                }
                else
                {
                    Console.Write("Already exist folder");
                }
            }
        }

        public static async Task UploadAsyn(FTPConfig fTPConfig,String folderName, string remoteFile, string localFile, CancellationTokenSource cts)
        {
            await Task.Run(() =>
            {
                try
                {
                    string desinationFolder = fTPConfig.BaseFolder + "/" + folderName;
                    UploadSFTPFile(fTPConfig.Host, fTPConfig.UserName, fTPConfig.Password, localFile, desinationFolder, fTPConfig.Port);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Upload SFTP failed:  - " + fTPConfig.BaseFolder + "/" + folderName + " - " + ex.ToString());
                    cts.Cancel();
                    deletedirectory(fTPConfig.Host, fTPConfig.UserName, fTPConfig.Password, fTPConfig.Port, fTPConfig.BaseFolder + "/" + folderName);
                }
            }, cts.Token);
        }

        public static async Task MoveAsyn(FTPConfig fTPConfig, string filePath, string newFilePath,string basePath)
        {
            await Task.Run(() =>
            {
                try
                {
                    MoveSFTPFile(fTPConfig.Host, fTPConfig.UserName, fTPConfig.Password, filePath, newFilePath, basePath, fTPConfig.Port);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IMPORTAN - MoveCsvToDone: " + filePath + " . Already import but move fail: " + ex.Message);
                }
            });
        }
    }
}
