﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace NCUtils.DataClass
{
    public class SheetsSample
    {
        static string[] Scopes = { SheetsService.Scope.Spreadsheets };
        private MySqlConnection connection;
        string siteName;
        string range;
        string sheetId;

        public SheetsSample(string _siteName, string _sheetId, string host,string db, string user, string pass, string range)
        {
            this.siteName = _siteName;
            this.sheetId = _sheetId;
            this.range = range;
            string connectionString;
            connectionString = "SERVER=" + host + ";" + "DATABASE=" +
            db + ";" + "UID=" + user + ";" + "PASSWORD=" + pass + ";";
            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Select statement
        //https://www.codeproject.com/Articles/43438/Connect-C-to-MySQL
        private IList<IList<Object>> SelectData(long lastId, bool isBingAds)
        {

            var query = "SELECT wp_posts.id as id, wp_posts.post_title as title, wp_posts.post_name as link, main_img_table.meta_value AS image_link, "
            + "replace(wp_posts.post_content,'\"',\"'\") as description,wp_posts_price.meta_value as price,wp_terms.name as product_type "
            + ",img1.meta_value as img1 "
            + ",img2.meta_value as img2 "
            + ",img3.meta_value as img3 "
            + ",img4.meta_value as img4 "
            + ",img5.meta_value as img5 "
            + "FROM wp_posts "
            + "LEFT JOIN wp_postmeta as wp_posts_price ON wp_posts.ID=wp_posts_price.post_ID "
            + "LEFT JOIN wp_postmeta as wp_meta2 ON wp_posts.ID = wp_meta2.post_ID "
            + "LEFT JOIN wp_postmeta AS main_img ON wp_posts.id=main_img.post_ID "
            + "LEFT JOIN wp_postmeta AS main_img_table ON main_img.meta_value=main_img_table.post_ID "
            + "LEFT JOIN wp_postmeta AS img1 ON SUBSTRING_INDEX(wp_meta2.meta_value, ',', 1) +1 = img1.post_ID "
            + "LEFT JOIN wp_postmeta AS img2 ON SUBSTRING_INDEX(wp_meta2.meta_value, ',', 1) +2= img2.post_ID "
            + "LEFT JOIN wp_postmeta AS img3 ON SUBSTRING_INDEX(wp_meta2.meta_value, ',', 1) +3= img3.post_ID "
            + "LEFT JOIN wp_postmeta AS img4 ON SUBSTRING_INDEX(wp_meta2.meta_value, ',', 1) +4= img4.post_ID "
            + "LEFT JOIN wp_postmeta AS img5 ON SUBSTRING_INDEX(wp_meta2.meta_value, ',', 1) +5= img5.post_ID "
            + "LEFT JOIN wp_term_relationships ON wp_term_relationships.object_id=wp_posts.ID "
            + "LEFT JOIN wp_term_taxonomy ON wp_term_taxonomy.term_taxonomy_id=wp_term_relationships.term_taxonomy_id "
            + "LEFT JOIN wp_terms ON wp_terms.term_id=wp_term_relationships.term_taxonomy_id "
            + "WHERE wp_posts.post_type='product' "
            + "AND wp_terms.name IN('Men Short-Sleeve T-Shirt','Heavy Blend Hoodie','Womens Jersey Tank') "
            + "AND wp_term_taxonomy.taxonomy='product_cat' "
            + "AND wp_meta2.meta_key ='_product_image_gallery' "
            + "AND wp_posts_price.meta_key='_price' "
            + "AND main_img_table.meta_key='_wp_attached_file' "
            + "AND img1.meta_key='_wp_attached_file' "
            + "AND img2.meta_key='_wp_attached_file' "
            + "AND img3.meta_key='_wp_attached_file' "
            + "AND img4.meta_key='_wp_attached_file' "
            + "AND img5.meta_key='_wp_attached_file' "
            + "AND main_img.meta_key='_thumbnail_id' "
            + "AND wp_posts.id > " + lastId + " "
            + "ORDER BY wp_posts.id ASC LIMIT ";
            query = (range != null && range != "") ? query + range : query + 100 ;

            List<IList<Object>> objNewRecords = new List<IList<Object>>();
            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {

                    IList<Object> obj = new List<Object>();

                    long id = Convert.ToInt64(dataReader["id"] + "");
                    Object found = objNewRecords.Find(x => Convert.ToInt64(x[0]) == id);
                    if(found != null)
                    {
                        break;
                    }
                    obj.Add(id);

                    string title = dataReader["title"] + "";
                    if (title != null && (title[0] == '+' || title[0] == '='))
                    {
                        title = title.Substring(1, title.Length - 1); ;
                    }
                    obj.Add(title);

                    string link = dataReader["link"] + "";
                    link = siteName + "/product/" + link;

                    obj.Add(link);

                    string imglnk = dataReader["image_link"] + "";
                    obj.Add(imglnk.IndexOf("http") <0 ? siteName+imglnk : imglnk);
                    obj.Add("in stock");

                    string desc = dataReader["description"] + "";
                    string desc2 = null;
                    try
                    {
                        desc2 = desc.Split(new string[] { "<select id='gearment_selectstyle'" }, StringSplitOptions.None)[0];
                    }catch (Exception) { }
                    string finalDesc = desc != null && desc.Length <= 0 ? desc : desc2;
                    if(finalDesc !=null && (finalDesc[0] == '+' || finalDesc[0] == '='))
                    {
                        finalDesc = finalDesc.Substring(1, finalDesc.Length - 1); ;
                    }
                    obj.Add(finalDesc);
                    obj.Add("Apparel & Accessories > Clothing > Shirts & Tops");
                    if (isBingAds)
                    {
                        obj.Add(dataReader["price"] + "");
                    }
                    else
                    {
                        obj.Add("USD" + dataReader["price"] + "");
                    }
                    obj.Add("");
                    obj.Add("");
                    obj.Add("new");
                    obj.Add("adult");
                    obj.Add("Black");
                    obj.Add("unisex");
                    obj.Add("M");
                    obj.Add(dataReader["product_type"] + "");
                    obj.Add("Apparel");
                    obj.Add(link);
                    obj.Add(link);
                    obj.Add("No");
                    obj.Add("186 g");
                    obj.Add("");
                    obj.Add("");

                    string img1 = dataReader["img1"] + "";
                    img1 = img1.IndexOf("http") < 0 ? siteName + img1 : img1;

                    string img2 = dataReader["img2"] + "";
                    img2 = img2.IndexOf("http") < 0 ? siteName + img2 : img2;

                    string img3 = dataReader["img3"] + "";
                    img3 = img3.IndexOf("http") < 0 ? siteName + img3 : img3;

                    string img4 = dataReader["img4"] + "";
                    img4 = img4.IndexOf("http") < 0 ? siteName + img4 : img4;

                    string img5 = dataReader["img5"] + "";
                    img5 = img5.IndexOf("http") < 0 ? siteName + img5 : img5;
                    //if (isBingAds)
                    {
                        string listLinks = "" + img1 + "," + img2 + "," + img3 + "," + img4 + "," + img5 + "";
                        obj.Add(listLinks);
                    }
                    //else
                    //{
                    //    obj.Add(img1);
                    //    obj.Add(img2);
                    //    obj.Add(img3);
                    //    obj.Add(img4);
                    //    obj.Add(img5);
                    //}                  

                    
                    objNewRecords.Add(obj);

                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                this.CloseConnection();
                //return list to be displayed
                return objNewRecords;
            }
            else
            {
                return objNewRecords;
            }
        }

        protected static string GetRange(SheetsService service, string SheetId)
        {
            // Define request parameters.
            String spreadsheetId = SheetId;
            String range = "A:A";
            SpreadsheetsResource.ValuesResource.GetRequest getRequest =
                       service.Spreadsheets.Values.Get(spreadsheetId, range);
            ValueRange getResponse = getRequest.Execute();
            IList<IList<Object>> getValues = getResponse.Values;
            int currentCount = getValues.Count + 2;
            String newRange = "A" + currentCount + ":A";
            return newRange;
        }


        public void StartFeeds(bool isBingAds)
        {
           
            SheetsService sheetsService = new SheetsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = GetCredential(),
                ApplicationName = "GoogleFeed",
            });

            // The ID of the spreadsheet to update.
            string spreadsheetId = sheetId;// "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms";  // TODO: Update placeholder value.

            // The A1 notation of a range to search for a logical table of data.
            // Values will be appended after the last row of the table.
            String range = "Sheet1!A1:A";

            //read last exported id
           long lastId = ReadByRange(spreadsheetId,range,sheetsService);

            var range2 = $"Sheet1!A:AB";
            IList<IList<object>> objNewRecords = SelectData(lastId, isBingAds);
            UpdatGoogleSheetinBatch(objNewRecords, spreadsheetId, range2, sheetsService);

        }

        private static void UpdatGoogleSheetinBatch(IList<IList<Object>> values, string spreadsheetId, string newRange, SheetsService service)
        {
            SpreadsheetsResource.ValuesResource.AppendRequest request =
               service.Spreadsheets.Values.Append(new ValueRange() { Values = values }, spreadsheetId, newRange);
            request.InsertDataOption = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            var response = request.Execute();
        }

        private long ReadByRange(String spreadsheetId, string range, SheetsService service)
        {
            SpreadsheetsResource.ValuesResource.GetRequest request = service.Spreadsheets.Values.Get(spreadsheetId, range);
            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 1)
            {
                return Convert.ToInt64(values[values.Count-1][0]);
            }
            else
            {
                return -1;
            }
        }

        public static UserCredential GetCredential()
        {
            UserCredential credential;

            using (var stream =  new FileStream(AppDomain.CurrentDomain.BaseDirectory + "assets/credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = AppDomain.CurrentDomain.BaseDirectory + "assets/token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            return credential;
        }
    }
}
