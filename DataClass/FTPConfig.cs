﻿using System;

namespace DataClass
{
    public class FTPConfig
    {
        private string host;
        private int port;
        private string baseFolder;
        private string userName;
        private string password;
        private string relateWebPath;
        private Boolean isFullPathExport;

        public FTPConfig(string host, int port, string baseFolder, string userName, string pass, bool isFullPathExport, string relateWebPath)
        {
            this.host = host;
            this.port = port;
            this.baseFolder = baseFolder;
            this.userName = userName;
            this.password = pass;
            this.isFullPathExport = isFullPathExport;
            this.relateWebPath = relateWebPath;
        }
        public string Host { get => host; set => host = value; }
        public string BaseFolder { get => baseFolder; set => baseFolder = value; }
        public string UserName { get => userName; set => userName = value; }
        public string Password { get => password; set => password = value; }
        public bool IsFullPathExport { get => isFullPathExport; set => isFullPathExport = value; }
        public int Port { get => port; set => port = value; }
        public string RelateWebPath { get => relateWebPath; set => relateWebPath = value; }
    }
}
