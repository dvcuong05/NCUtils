﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Requests;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using NCUtils.DataClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MockupTool.classes
{
    class GoogleDriveBackup
    {
        string merchFolderId = "1Y6Y_3FVKXSJ2rcG7sZf1mkyfHiecZs76";
        string teescapeFolderId = "1HU8sFqH4AgKdSi-qYp48aHkij9kI2JT9";
        string teeSpringFolderId = "1uN5QKiU-jxMhhqJjYFZTiIlxEVGPvOWZ";
        string mixMockupId = "1iCHagQl3BRhTk0m-vO1R9d0z4-zfiusP";
        string orderFolderId = "1kywljtn4T63QhPrnHK3v537Ha9vYYkDU";


        public string CURRENT_APP_NAME = "mixmockup";

        internal static string[] Scopes = { DriveService.Scope.Drive };
        internal static string ApplicationName = "NCSoftware Google Drive";
        private string ggAuthenFolder = AppDomain.CurrentDomain.BaseDirectory + "assets/configs";
        DriveService service;

        HashSet<string> FILES_QUEUE= new HashSet<string>();

        private UserCredential GetCredentials()
        {
            try
            {
                UserCredential credential;
                string credentialsFile = ggAuthenFolder + "/gappcache.bat";
                using (var stream = new FileStream(credentialsFile, FileMode.Open, FileAccess.Read))
                {
                    string credPath = ggAuthenFolder;
                    credPath = Path.Combine(credPath, "");
                    try
                    {
                        credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                            GoogleClientSecrets.FromStream(stream).Secrets,
                            Scopes,
                            "user",
                            CancellationToken.None,
                            new FileDataStore(credPath, true)).Result;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("GetCredentials() has an exception." + ex.Message);
                        return null;
                    }
                }
                return credential;
            }
            catch (Exception ex)
            {
                Console.WriteLine("could not get gappcahe file", ex.Message);
                return null;
            }
        }

        private string getParentFolderId(string applicationName)
        {
            string parentId = merchFolderId;
            if (applicationName.ToLower() == "teescape")
            {
                parentId = teescapeFolderId;
            }
            else if (applicationName == "teespring")
            {
                parentId = teeSpringFolderId;
            }else if(applicationName == CURRENT_APP_NAME)
            {
                parentId = mixMockupId;
            }
            return parentId;
        }

        public async Task uploadFolder(string folderPath, List<DriveFileItem> coupleFileKeyDone)
        {
            UserCredential credential;
            credential = GetCredentials();

            // Create Drive API service.
            service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            CancellationTokenSource cts = new CancellationTokenSource();
            string parentId = getParentFolderId(this.CURRENT_APP_NAME);
            string folderid = orderFolderId;

            string[] files = Directory.GetFiles(folderPath);
            int i = 0;
            do
            {
                List<Task> allUploadTasks = new List<Task>();
                for (int j = 0; j < 15; j++)
                {
                    if (i < files.Length)
                    {
                        allUploadTasks.Add(UploadFileToFolder(files[i], coupleFileKeyDone, service, folderid, cts));
                    }
                    i += 1;
                }
                await Task.WhenAll(allUploadTasks.ToArray());
            } while (i < files.Length);
        }

        private string checkFileExist(DriveService service, string parentId, string fileName, ref string pageToken)
        {
            try
            {
                // Define parameters of request.
                FilesResource.ListRequest listRequest = service.Files.List();
                listRequest.PageSize = 100;
                //listRequest.Fields = "nextPageToken, files(id, name)";
                listRequest.Fields = "nextPageToken, files(id,name)";
                listRequest.PageToken = pageToken;
                //listRequest.Q = "mimeType='"+ mineType + "' and parents='" + parentId + "'";
                listRequest.Q = "parents='" + parentId + "'";
                // List files.
                var request = listRequest.Execute();

                if (request.Files != null && request.Files.Count > 0)
                {
                    foreach (var file in request.Files)
                    {
                        if (file.Name == Path.GetFileName(fileName))
                        {
                            return file.Id;
                        }
                    }

                    pageToken = request.NextPageToken;

                    if (request.NextPageToken != null)
                    {
                        Console.ReadLine();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Opps! An exception is occured " + ex.Message + "\n.Please try to close app and re-try", null);
                return null;
            }
        }

        public async Task UploadSingleFile(string filePath, bool isFirstItemInFolder, bool deleteAfterDone = false)
        {
            try
            {
                if (!isFirstItemInFolder)
                {
                    FILES_QUEUE.Add(filePath);
                    return;
                }

                string folderName = Path.GetDirectoryName(filePath);
                string fileName = Path.GetFileName(filePath);

                UserCredential credential;
                credential = GetCredentials();

                CancellationTokenSource cts = new CancellationTokenSource();
                // Create Drive API service.
                DriveService service = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = ApplicationName,
                });
                string parentId = getParentFolderId(this.CURRENT_APP_NAME);
                string folderid = null;
                string pageToken = null;

                folderid = getParentFolderId(this.CURRENT_APP_NAME);
                // Tam thoi ko up hinh theo tung folder... gom het vo mixmockup
                //if (folderName.Contains("assets") || folderName.Contains("setting") || fileName.Contains("setting"))
                //{
                //    folderid = ASSET_FOLDERS[this.CURRENT_APP_NAME];
                //}
                //if (folderid == null)
                //{
                //    do
                //    {
                //        folderid = checkFolderExist(service, parentId, folderName, ref pageToken);

                //    } while (pageToken != null);

                //    if (folderid == null)
                //    {
                //        folderid = createDriveFolder(parentId, folderName, folderName, service);
                //    }
                //}

                //Console.WriteLine("Upload a item " + path);
                await Task.Run(() =>
                {
                    var fileMetadata = new Google.Apis.Drive.v3.Data.File();
                    fileMetadata.Name = Path.GetFileName(filePath);
                    fileMetadata.MimeType = Path.GetExtension(filePath).ToLower() == ".txt" || Path.GetExtension(filePath).ToLower() == ".json" ? "text/plain" : "image/*";

                    fileMetadata.Parents = new List<string>
                {
                    folderid
                };

                    string fileId = null;
                    pageToken = null;
                    do
                    {
                        fileId = checkFileExist(service, folderid, filePath, ref pageToken);
                    } while (pageToken != null);

                    if (fileId != null)
                    {
                        DeleteFile(service, fileId);
                    }
                    FilesResource.CreateMediaUpload request;
                    using (var stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        request = service.Files.Create(fileMetadata, stream, fileMetadata.MimeType);
                        request.Fields = "id";
                        request.Upload();
                    }
                    var file = request.ResponseBody;

                    FILES_QUEUE.Remove(filePath);
                    if (FILES_QUEUE.Count > 0)
                    {
                        Task.Run(() => this.UploadSingleFile(FILES_QUEUE.First(), true, false));
                    }
                }, cts.Token);
            }
            catch (Exception ex)
            {
                Console.WriteLine("***VNCUD Upload single file has exception:", ex.Message);
            }
        }

        public async Task UploadFileToFolder(string filePath, List<DriveFileItem> coupleFileKeyDone, DriveService service, string folderId, CancellationTokenSource cts)
        {
            await Task.Run(() =>
            {
                var fileMetadata = new Google.Apis.Drive.v3.Data.File();
                fileMetadata.Name = Path.GetFileName(filePath);
                fileMetadata.MimeType = Path.GetExtension(filePath).ToLower() == ".txt" || Path.GetExtension(filePath).ToLower() == ".json" ? "text/plain" : "image/*";

                fileMetadata.Parents = new List<string>
                 {
                    folderId
                 };

                FilesResource.CreateMediaUpload request;
                using (var stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    request = service.Files.Create(fileMetadata, stream, fileMetadata.MimeType);
                    request.Fields = "id";
                    request.Upload();
                }
                var file = request.ResponseBody;
                coupleFileKeyDone.Add(new DriveFileItem(Path.GetFileNameWithoutExtension(filePath), file.Id));

                // Share file
                try
                {
                    Permission userPermission = new Permission();
                    userPermission.Type = "anyone";
                    userPermission.Role = "reader";
                    //userPermission.View = "published";
                    var permissionRequest = service.Permissions.Create(userPermission, file.Id);
                    permissionRequest.Execute();
                }
                catch(Exception ee)
                {
                    Console.WriteLine("Cannot share permission " + ee.Message);
                }

            }, cts.Token);
        }

        public static void DeleteFile(DriveService service, String fileId)
        {
            try
            {
                service.Files.Delete(fileId).Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred GG DeleteFile: " + e.Message);
            }
        }
    }
}
