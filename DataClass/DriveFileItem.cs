﻿
namespace NCUtils.DataClass
{
    public class DriveFileItem
    {
        string name;
        string fileId;

        public string Name { get => name; set => name = value; }
        public string FileId { get => fileId; set => fileId = value; }

        public DriveFileItem(string n, string id)
        {
            name = n;
            fileId = id;
        }
    }
}
