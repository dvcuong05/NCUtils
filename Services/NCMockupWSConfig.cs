﻿
namespace NCUtils.Services
{
    public class NCMockupWSConfig
    {
        int width = 3600;
        int height = 4800;
        float dpix = 300;
        float dpiy = 300;
        string outputFolder;

        public int Width { get => width; set => width = value; }
        public int Height { get => height; set => height = value; }
        public float Dpix { get => dpix; set => dpix = value; }
        public float Dpiy { get => dpiy; set => dpiy = value; }
        public string OutputFolder { get => outputFolder; set => outputFolder = value; }
    }
}
