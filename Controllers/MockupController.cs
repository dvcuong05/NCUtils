﻿using NCUtils.Models;
using NCUtils.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;

namespace NCUtils.Controllers
{
    public class MockupController : ApiController
    {
        // GET: api/Mockup
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Mockup/5
        public string Get(int id)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(@"C:\Program Files (x86)\NCSoftware\NCMixMockup\MockupTool.exe");
            System.Diagnostics.Process.Start(startInfo);
            
            return "value";
        }

        // POST: api/Mockup
        public string Post([FromBody]MockupRequestData data)
        {
            if(data != null)
            {
                if (!checkURLExist(data.ImageUri))
                {
                    return "404 - URL not found:"+data.ImageUri; // 404 file not found
                }
                Task.Run(() => ExportMockupByWS.exportImageItemAsync(data));
                return "OK - 200"; // OK = 200
            }
            return "400 - The request data is empty." + data;
        }

        // PUT: api/Mockup/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Mockup/5
        public void Delete(int id)
        {
        }

        public bool checkURLExist(string url)
        {
            bool Test = true;
            Uri urlCheck = new Uri(url);
            System.Net.WebRequest request = System.Net.WebRequest.Create(urlCheck);
            request.Timeout = 5000;
            try
            {
                request.GetResponse();
            }
            catch (Exception exx)
            {
                Test = false;
            }
            return Test;
        }
    }
}
